package fr.dawan.formation.bibliotheque.services;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import fr.dawan.formation.bibliotheque.dto.LivreDto;
import fr.dawan.formation.bibliotheque.entities.Livre;
import fr.dawan.formation.bibliotheque.repositories.LivreRepository;

@Service
public class LivreServiceImplement implements LivreService {

    @Autowired
    private ModelMapper mapper;
    
    @Autowired
    private LivreRepository repository;
    
    @Override
    public List<LivreDto> getAllLivre() {
        return repository.findAll().stream().map(l->mapper.map(l,LivreDto.class)).collect(Collectors.toList());
    }

    @Override
    public List<LivreDto> getAllLivre(Pageable page) {
        return repository.findAll(page).stream().map(l->mapper.map(l,LivreDto.class)).collect(Collectors.toList());
    }

    @Override
    public LivreDto findById(long id) {
        return mapper.map(repository.findById(id).get(), LivreDto.class);
    }

    @Override
    public List<LivreDto> searchByTitre(String titre) {
        return repository.findByTitreLikeIgnoreCase("%"+titre+"%").stream().map(l->mapper.map(l,LivreDto.class)).collect(Collectors.toList());
    }

    @Override
    public void deleteById(long id) {
        repository.deleteById(id);
        
    }

    @Override
    public LivreDto saveOrUpdate(LivreDto lvrDto) {
        Livre l=repository.saveAndFlush(mapper.map(lvrDto, Livre.class));
        return mapper.map(l, LivreDto.class);
    }
}
