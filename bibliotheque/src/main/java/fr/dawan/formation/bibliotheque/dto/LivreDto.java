package fr.dawan.formation.bibliotheque.dto;

public class LivreDto {

    private long id;

    private String titre;

    private int annee;

    private CategorieDto categorie;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public int getAnnee() {
        return annee;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }

    public CategorieDto getCategorie() {
        return categorie;
    }

    public void setCategorie(CategorieDto categorie) {
        this.categorie = categorie;
    }

    @Override
    public String toString() {
        return "LivreDto [id=" + id + ", titre=" + titre + ", annee=" + annee + ", categorie=" + categorie + "]";
    }

}
