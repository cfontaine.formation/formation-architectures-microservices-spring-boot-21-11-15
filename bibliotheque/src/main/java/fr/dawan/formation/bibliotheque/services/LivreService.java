package fr.dawan.formation.bibliotheque.services;

import java.util.List;

import org.springframework.data.domain.Pageable;

import fr.dawan.formation.bibliotheque.dto.LivreDto;

public interface LivreService {
    List<LivreDto> getAllLivre();

    List<LivreDto> getAllLivre(Pageable page);

    LivreDto findById(long id);

    List<LivreDto> searchByTitre(String titre);

    void deleteById(long id);

    LivreDto saveOrUpdate(LivreDto lvrDto);

}
