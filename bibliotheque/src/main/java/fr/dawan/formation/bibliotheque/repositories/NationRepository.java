package fr.dawan.formation.bibliotheque.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.dawan.formation.bibliotheque.entities.Nation;

@Repository
public interface NationRepository extends JpaRepository<Nation, Long> {
    
        List<Nation> findByNom(String nom);
        
}
