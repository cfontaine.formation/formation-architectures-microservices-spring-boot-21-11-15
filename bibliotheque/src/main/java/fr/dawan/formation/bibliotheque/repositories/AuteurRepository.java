package fr.dawan.formation.bibliotheque.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.dawan.formation.bibliotheque.entities.Auteur;

@Repository
public interface AuteurRepository extends JpaRepository<Auteur, Long> {

    List<Auteur> findByDecesIsNull();
    
    @Query(value="FROM Auteur a WHERE a.deces is null")
    Page<Auteur> findAlive(Pageable pageable);
    
    List<Auteur> findByNom(String nom);
    
    @Query(value="FROM Auteur a JOIN a.livres l WHERE l.id=:idLivre")
    List<Auteur> findByLivreId(@Param("idLivre")long id);
    
    @Query(value="SELECT auteurs.id, auteurs.prenom, auteurs.nom,auteurs.DECES, auteurs.NAISSANCE, auteurs.VERSION , auteurs.NATION_ID FROM auteurs "
            + "INNER JOIN  livres_auteurs ON auteurs.id=livres_auteurs.auteurs_id "
            + "GROUP BY auteurs.id ORDER BY COUNT(livres_auteurs.livres_id) DESC LIMIT 5"
            ,nativeQuery = true)
    List<Auteur> findTop5CountLivre();

}
