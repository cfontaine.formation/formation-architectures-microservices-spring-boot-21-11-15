package fr.dawan.formation.bibliotheque.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.dawan.formation.bibliotheque.entities.Livre;

@Repository
public interface LivreRepository extends JpaRepository<Livre, Long> {
    List<Livre> findByTitreLikeIgnoreCase(String titre);

    List<Livre> findByAnnee(int anneSortie);

    List<Livre> findByAnneeBetween(int min, int max);

}
