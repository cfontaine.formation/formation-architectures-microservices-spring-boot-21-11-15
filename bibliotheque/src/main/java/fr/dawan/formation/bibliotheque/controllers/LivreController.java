package fr.dawan.formation.bibliotheque.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.dawan.formation.bibliotheque.dto.LivreDto;
import fr.dawan.formation.bibliotheque.services.LivreService;

@RestController
@RequestMapping("api/livres")
public class LivreController {

    @Autowired
    LivreService service;

    @GetMapping(produces = "application/json")
    public List<LivreDto> getAllLivres() {
        return service.getAllLivre();
    }

    @GetMapping(params = { "page", "size" }, produces = "application/json")
    public List<LivreDto> getAllPLivreDtoPage(Pageable page) {
        return service.getAllLivre(page);
    }

    @GetMapping(value = "/{id:[0-9]+}", produces = "application/json")
    public LivreDto getById(@PathVariable long id) {
        return service.findById(id);
    }

    @GetMapping(value = "/{titre:[a-zA-Z]+}", produces = "application/json")
    public List<LivreDto> getByTitre(@PathVariable String titre) {
        return service.searchByTitre(titre);
    }

    @DeleteMapping(value = "/{id}", produces = "text/plain")
    public String remove(@PathVariable long id) {
        service.deleteById(id);
        return "Le livre id=" + id + " est supprimé";
    }

    @PostMapping(consumes = "application/json", produces = "application/json")
    public LivreDto save(@RequestBody LivreDto lvrDto) {
        return service.saveOrUpdate(lvrDto);
    }

    @PutMapping(consumes = "application/json", produces = "application/json")
    public LivreDto update(@RequestBody LivreDto lvrDto) {
        return service.saveOrUpdate(lvrDto);
    }

}
