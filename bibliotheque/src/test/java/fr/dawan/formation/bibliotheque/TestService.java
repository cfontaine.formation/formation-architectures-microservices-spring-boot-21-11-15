package fr.dawan.formation.bibliotheque;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import fr.dawan.formation.bibliotheque.dto.LivreDto;
import fr.dawan.formation.bibliotheque.services.LivreService;

@SpringBootTest
@ActiveProfiles("DEV")
public class TestService {
    @Autowired
    LivreService service;

    @Test
    void testServiceLivre() {
        
        LivreDto l=new LivreDto();
        l.setAnnee(2021);
        l.setTitre("test");
        l=service.saveOrUpdate(l);
        System.out.println(service.findById(l.getId()));
        System.out.println(service.searchByTitre("test"));
        service.deleteById(l.getId());
        service.getAllLivre().forEach(System.out::println);
    }
    
    
    
    
}
