package fr.dawan.formation.bibliotheque;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;

import fr.dawan.formation.bibliotheque.entities.Auteur;
import fr.dawan.formation.bibliotheque.entities.Livre;
import fr.dawan.formation.bibliotheque.entities.Nation;
import fr.dawan.formation.bibliotheque.repositories.AuteurRepository;
import fr.dawan.formation.bibliotheque.repositories.LivreRepository;
import fr.dawan.formation.bibliotheque.repositories.NationRepository;

@SpringBootTest
@ActiveProfiles("DEV")
public class TestRepositoryBiblio {

    @Autowired
    NationRepository nationRepo;
    
    @Autowired
    AuteurRepository auteurRepo;
    
    @Autowired
    LivreRepository livreRepo;
    
    
    @Test
    public void testNation() {
        List<Nation> nations= nationRepo.findByNom("Belgique");
        for(Nation n:nations) {
            System.out.println(n);
        }
        
        System.out.println("________________________");
        List<Auteur> lst=auteurRepo.findByDecesIsNull();
        for(Auteur au:lst) {
            System.out.println(au);
        }
        
        System.out.println("________________________");
        Page<Auteur> pa=auteurRepo.findAlive(PageRequest.of(1, 2));
        System.out.println(pa.getTotalElements() + " " + pa.getTotalPages());
        for(Auteur au:pa.getContent()) {
            System.out.println(au);
        }

        System.out.println("________________________");
        List<Auteur> lstA=auteurRepo.findByNom("Gibson");
        for(Auteur au:lstA) {
            System.out.println(au);
        }
        
        System.out.println("________________________");
        lstA=auteurRepo.findByLivreId(133);
        for(Auteur au:lstA) {
            System.out.println(au);
        }
        
        System.out.println("________________________");
        lstA=auteurRepo.findTop5CountLivre();
        for(Auteur au:lstA) {
            System.out.println(au);
        }
        
        System.out.println("________________________");
        List<Livre> lstL=livreRepo.findByTitreLikeIgnoreCase("du%");
        for(Livre l:lstL) {
            System.out.println(l);
        }
        
        System.out.println("________________________");
        lstL=livreRepo.findByAnnee(1982);
        for(Livre l:lstL) {
            System.out.println(l);
        }
        
        livreRepo.findByAnneeBetween(1959, 1970).forEach(System.out::println);
    }
    
    
    
}
