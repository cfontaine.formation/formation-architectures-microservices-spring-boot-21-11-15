package fr.dawan.formation.springboot.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import fr.dawan.formation.springboot.dto.MarqueDto;
import fr.dawan.formation.springboot.services.MarqueService;

@Controller
public class HelloWorldController {
    
    @Autowired
    MarqueService service;
    @Autowired
    PasswordEncoder encoder;
    
  @Value("${message}")
    private String msg;

    @RequestMapping("/")
    public String helloWorld() {
        return "message";
    }
    
    @RequestMapping(value="/mvc",method=RequestMethod.GET)
    public String exempleMvc(Model model) {
        MarqueDto m=service.getMarqueById(2);
        model.addAttribute("marque", m);
        return "exemplevue";
    }
    
//    @GetMapping(value="/genpassword")
//    public String genPassword(@RequestParam  String password) {
//        return encoder.encode(password);
//    }
}