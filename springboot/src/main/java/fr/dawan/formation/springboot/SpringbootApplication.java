package fr.dawan.formation.springboot;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;


@SpringBootApplication
public class SpringbootApplication {

	public static void main(String[] args) {
	    SpringApplication app=new SpringApplication(SpringbootApplication.class);
	    
//	    app.setBanner(new Banner() {
//         
//            @Override
//            public void printBanner(Environment environment, Class<?> sourceClass, PrintStream out) {
//                out.println("Dawan");
//            }
//        });
//	    app.setBannerMode(Mode.OFF);
	    app.run(args);
	    
//	    SpringApplication.run(SpringbootApplication.class, args);
	}

	@Bean
	ModelMapper modelMapper() {
	    return new ModelMapper();
	}
	
	@Bean
	public PasswordEncoder passwordEncoder() {
	    return new BCryptPasswordEncoder();
	}
}
