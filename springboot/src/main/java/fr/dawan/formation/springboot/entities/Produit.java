package fr.dawan.formation.springboot.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import fr.dawan.formation.springboot.enums.Conditionnement;

@Entity
@Table(name = "produits")
public class Produit implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Column(length = 60)
    private String description;
    
    
    private double prix;
    
    @Enumerated(EnumType.STRING)
    private Conditionnement conditionnnement;
    
    @Transient
    private int nepaspersister;
    
    @Lob
    private byte[] image;
    
    public Produit() {

    }
    
    @ElementCollection(targetClass = String.class)
    @CollectionTable(name="commentaires_produits")
    private List<String> commentaires;

    @ManyToOne
    @JoinColumn(name="id_marque",referencedColumnName = "id")
    private Marque marque;
    
    @ManyToMany
    @JoinTable(name = "produits2fournisseurs",
    joinColumns = @JoinColumn(name="id_produit",referencedColumnName = "id"),
    inverseJoinColumns = @JoinColumn(name="id_fournisseur",referencedColumnName = "id"))
    private List<Fournisseur> fournisseurs;
    
    public Produit(String description, double prix) {
        this.description = description;
        this.prix = prix;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }
    
    

    public Conditionnement getConditionnnement() {
        return conditionnnement;
    }

    public void setConditionnnement(Conditionnement conditionnnement) {
        this.conditionnnement = conditionnnement;
    }

    public List<String> getCommentaires() {
        return commentaires;
    }

    public void setCommentaires(List<String> commentaires) {
        this.commentaires = commentaires;
    }

    
    public Marque getMarque() {
        return marque;
    }

    public void setMarque(Marque marque) {
        this.marque = marque;
    }
    
    

    public List<Fournisseur> getFournisseurs() {
        return fournisseurs;
    }

    public void setFournisseurs(List<Fournisseur> fournisseurs) {
        this.fournisseurs = fournisseurs;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Produit [id=" + id + ", description=" + description + ", prix=" + prix + "]";
    }

}
