package fr.dawan.formation.springboot.dto;

public class FournisseurDto {

    private long id;

    private String nom;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "FournisseurDTO [id=" + id + ", nom=" + nom + "]";
    }

}
