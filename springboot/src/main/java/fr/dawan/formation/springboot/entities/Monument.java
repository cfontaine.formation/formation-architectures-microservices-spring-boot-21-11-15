package fr.dawan.formation.springboot.entities;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="monuments")
public class Monument extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @Column(nullable = false)
    private String nom;
 
    @Column(nullable = false)
    private int anneeConstruction;

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name = "id_coordonnee", referencedColumnName = "id")
    private Coordonnee coordonnee;
    
    @ManyToOne(cascade=CascadeType.ALL)
    private Nation nation;
    
    @ManyToMany
    private Set<Etiquette> etiquettes;

    public Monument() {
    }

    public Monument(String nom, int anneeConstruction, Coordonnee coordonnee, Nation nation) {
        this.nom = nom;
        this.anneeConstruction = anneeConstruction;
        this.coordonnee = coordonnee;
        this.nation = nation;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getAnneeConstruction() {
        return anneeConstruction;
    }

    public void setAnneeConstruction(int anneeConstruction) {
        this.anneeConstruction = anneeConstruction;
    }

    public Coordonnee getCoordonnee() {
        return coordonnee;
    }

    public void setCoordonnee(Coordonnee coordonnee) {
        this.coordonnee = coordonnee;
    }

    public Nation getNation() {
        return nation;
    }

    public void setNation(Nation nation) {
        this.nation = nation;
    }

    public Set<Etiquette> getEtiquettes() {
        return etiquettes;
    }

    public void setEtiquettes(Set<Etiquette> etiquettes) {
        this.etiquettes = etiquettes;
    }

    @Override
    public String toString() {
        return "Monument [nom=" + nom + ", anneeConstruction=" + anneeConstruction + ", coordonnee=" + coordonnee + "]";
    }
    
}
