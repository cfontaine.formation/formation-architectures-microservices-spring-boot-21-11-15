package fr.dawan.formation.springboot.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

public class AuthProvider extends DaoAuthenticationProvider {

    @Autowired
    UserDetailsService service;
    
    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        UsernamePasswordAuthenticationToken tkAuth = (UsernamePasswordAuthenticationToken) authentication;
        String userName = tkAuth.getName();
        String userPassword = tkAuth.getCredentials().toString();
        UserDetails userDb = service.loadUserByUsername(userName);
        if (userPassword != null && passwordEncoder.matches(userPassword, userDb.getPassword())){//userPassword.equals(userDb.getPassword())) {
            return new UsernamePasswordAuthenticationToken(userDb, null, userDb.getAuthorities());
        } else {
            throw new BadCredentialsException(userName + ": Mauvais mot de passe");
        }

    }

    @Override
    public boolean supports(Class<?> authentication) {
        // TODO Auto-generated method stub
        return true;
    }

}
