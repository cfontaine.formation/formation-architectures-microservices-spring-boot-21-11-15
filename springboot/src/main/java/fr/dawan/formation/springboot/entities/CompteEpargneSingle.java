package fr.dawan.formation.springboot.entities;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("COMPTE_EPARGNE")
public class CompteEpargneSingle extends CompteBancaireSingle {

    private static final long serialVersionUID = 1L;

    private double taux=0.5;

    public CompteEpargneSingle() {
        super();

    }

    public CompteEpargneSingle(String titulaire, String iban, double solde,double taux) {
        super(titulaire, iban, solde);
        this.taux=taux;
  }

    public double getTaux() {
        return taux;
    }

    public void setTaux(double taux) {
        this.taux = taux;
    }

    @Override
    public String toString() {
        return "CompteEpargne [taux=" + taux + ", toString()=" + super.toString() + "]";
    }
    
}
