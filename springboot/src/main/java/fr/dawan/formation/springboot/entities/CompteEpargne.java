package fr.dawan.formation.springboot.entities;
import javax.persistence.Entity;

@Entity
public class CompteEpargne extends CompteBancaire {

    private static final long serialVersionUID = 1L;

    private double taux=0.5;

    public CompteEpargne() {
        super();

    }

    public CompteEpargne(String titulaire, String iban, double solde,double taux) {
        super(titulaire, iban, solde);
        this.taux=taux;
  }

    public double getTaux() {
        return taux;
    }

    public void setTaux(double taux) {
        this.taux = taux;
    }

    @Override
    public String toString() {
        return "CompteEpargne [taux=" + taux + ", toString()=" + super.toString() + "]";
    }
    
}
