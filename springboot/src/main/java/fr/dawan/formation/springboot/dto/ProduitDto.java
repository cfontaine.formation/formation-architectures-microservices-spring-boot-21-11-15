package fr.dawan.formation.springboot.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ProduitDto {
    private long id;

    @NotBlank
    @Size(max = 255)
    private String description;

    
    private double prix;

    @NotNull
    private MarqueDto marque;

 //   private List<FournisseurDto> fournisseurs;

    private byte[] image;
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public MarqueDto getMarque() {
        return marque;
    }

    public void setMarque(MarqueDto marque) {
        this.marque = marque;
    }

//    public List<FournisseurDto> getFournisseurs() {
//        return fournisseurs;
//    }
//
//    public void setFournisseurs(List<FournisseurDto> fournisseurs) {
//        this.fournisseurs = fournisseurs;
//    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "ProduitDto [id=" + id + ", description=" + description + ", prix=" + prix + "]";
    }
}
