package fr.dawan.formation.springboot.entities;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "nations")
public class Nation extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @Column(nullable = false, length = 100)
    private String nom;

    @OneToMany(mappedBy = "nation")
    private Set<Monument> monuments;

    public Nation() {
    }

    public Nation(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Set<Monument> getMonuments() {
        return monuments;
    }

    public void setMonuments(Set<Monument> monuments) {
        this.monuments = monuments;
    }

    @Override
    public String toString() {
        return "Nation [nom=" + nom + "]";
    }
}
