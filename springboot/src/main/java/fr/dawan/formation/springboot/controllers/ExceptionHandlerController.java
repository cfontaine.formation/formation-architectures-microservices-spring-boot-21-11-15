package fr.dawan.formation.springboot.controllers;

import java.io.IOException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import fr.dawan.formation.springboot.utils.ApiError;

@ControllerAdvice
public class ExceptionHandlerController extends ResponseEntityExceptionHandler {
    
    
    @ExceptionHandler(value = {Exception.class})
    public ResponseEntity<Object> handlerExcetion(Exception e,WebRequest request) {
        ApiError err=new ApiError(HttpStatus.I_AM_A_TEAPOT, e.getMessage());
        return handleExceptionInternal(e, err, new HttpHeaders(), HttpStatus.I_AM_A_TEAPOT, request);
    }

}
