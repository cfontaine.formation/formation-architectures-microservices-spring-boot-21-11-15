package fr.dawan.formation.springboot.dto;

public class MarqueDto {

    private long id;
    
    private String nom;
    
    

    public MarqueDto() {
    }



    public MarqueDto(String nom) {
        this.nom = nom;
    }



    public long getId() {
        return id;
    }



    public void setId(long id) {
        this.id = id;
    }



    public String getNom() {
        return nom;
    }



    public void setNom(String nom) {
        this.nom = nom;
    }



    @Override
    public String toString() {
        return "MarqueDto [id=" + id + ", nom=" + nom + "]";
    }
    
    
}
