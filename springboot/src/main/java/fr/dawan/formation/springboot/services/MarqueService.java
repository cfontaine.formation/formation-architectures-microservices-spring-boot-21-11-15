package fr.dawan.formation.springboot.services;

import java.util.List;

import org.springframework.data.domain.Pageable;

import fr.dawan.formation.springboot.dto.MarqueDto;

public interface MarqueService {

    List<MarqueDto> getAllMarque();
    
    List<MarqueDto> getAllMarque(Pageable page);
    
    MarqueDto getMarqueById(long id);
    
    MarqueDto saveOrUpdate(MarqueDto md);
    
    void deleteById(long id);
}
