package fr.dawan.formation.springboot.repositories;

import java.util.List;

import fr.dawan.formation.springboot.entities.Employe;


public interface EmployeCustomRepository {
    
    List<Employe> findBy(String prenom,String nom);

}
