package fr.dawan.formation.springboot.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.dawan.formation.springboot.entities.Produit;

@Repository
public interface ProduitRepository extends JpaRepository<Produit, Long> {
    
   List<Produit> findByPrixLessThanEqualOrderByPrix(double prix);
   
   List<Produit> findByPrixBetween(double min, double max);
   
   List<Produit> findByDescriptionLikeIgnoreCase(String description);
   
   @Query(nativeQuery = true,value = "SELECT * FROM produits WHERE prix<:montant")
   List<Produit> findByPrixInf(@Param("montant")double montantMax);
   
   @Query(value = "SELECT p FROM Produit p WHERE p.prix<:montant")
   List<Produit> findByPrixInfJPQL(@Param("montant")double montantMax);
   
   Page<Produit> findByPrixLessThanEqual(double prix,Pageable pageable);
   List<Produit> findByPrixLessThanEqual(double prix,Sort sort);
   
   List<Produit> findTop3ByPrixLessThan(double prix);
   
   @Query(value="CALL GET_COUNT_BY_PRIX(:montant)",nativeQuery=true)
   int countByPrixSQL(@Param("montant") double montant);
   
   @Procedure(value="GET_COUNT_BY_PRIX2")
   int countByPrix(double montant);
   
}
