package fr.dawan.formation.springboot.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    
    @Autowired 
    private UserDetailsService service;
    
    @Autowired
    private AuthenticationProvider provider;

    @Bean
    public AuthenticationProvider getProvider() {
        AuthProvider provider=new AuthProvider();
        provider.setUserDetailsService(service);
        return provider;
    }
    
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(service);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
        .authenticationProvider(provider)
        .authorizeRequests()
        .antMatchers(HttpMethod.GET,"api/produits").hasAnyAuthority("READ","SUPER")
        .antMatchers(HttpMethod.POST,"/api/produits").hasAnyAuthority("WRITE","SUPER")
        .antMatchers(HttpMethod.PUT,"/api/produits").hasAnyAuthority("WRITE","SUPER")
        .antMatchers(HttpMethod.DELETE,"/api/produits").hasAnyAuthority("DELETE","SUPER")
        .anyRequest().permitAll()
        .and()
        .httpBasic();
    }

}

