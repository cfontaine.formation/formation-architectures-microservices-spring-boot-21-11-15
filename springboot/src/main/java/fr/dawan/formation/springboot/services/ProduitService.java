package fr.dawan.formation.springboot.services;

import java.util.List;

import org.springframework.data.domain.Pageable;

import fr.dawan.formation.springboot.dto.ProduitDto;

public interface ProduitService {

    List<ProduitDto> getAllProduit(Pageable page);

    ProduitDto getProduitById(long id);
    
    List<ProduitDto> getProduitByDescription(String Description);
    
    void deleteProduit(long id);
    
    ProduitDto saveOrUpdate(ProduitDto prod);
    
}
