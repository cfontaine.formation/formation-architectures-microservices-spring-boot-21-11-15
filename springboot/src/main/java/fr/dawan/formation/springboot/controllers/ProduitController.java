package fr.dawan.formation.springboot.controllers;

import java.io.IOException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import fr.dawan.formation.springboot.dto.ProduitDto;
import fr.dawan.formation.springboot.services.ProduitService;

@RestController
@RequestMapping("api/produits")
public class ProduitController {
    
    @Autowired
    ProduitService service;

    @GetMapping(produces="application/json")
    public List<ProduitDto> getAllProduit(){
        return service.getAllProduit(Pageable.unpaged());
    }
    
    @GetMapping(params= {"page","size"},produces="application/json")
    public List<ProduitDto> getAllProduitPage(Pageable page){
        return service.getAllProduit(page);
    }
    
    @GetMapping(value="/{id:[0-9]+}",produces="application/json")
    public ProduitDto getById(@PathVariable long id) {
        return service.getProduitById(id);
    }
    
    @GetMapping(value="/{description:[a-zA-Z]+}",produces="application/json")
    public List<ProduitDto> getById(@PathVariable String description) {
        return service.getProduitByDescription(description);
    }  
    
    
    @DeleteMapping(value="/{id}", produces ="text/plain")
    public ResponseEntity<?> remove(@PathVariable long id){
       // try {
            service.deleteProduit(id);
            return new ResponseEntity<String>(HttpStatus.OK);
       // }catch(Exception e) {
        //    return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
       // }
        //return "Le produit id="+ id +" est supprimé";
    }
    
    @PostMapping(consumes="application/json",produces="application/json")
    public ProduitDto save(@Valid @RequestBody ProduitDto prodDto) {
        return service.saveOrUpdate(prodDto);
    }
    
    @PostMapping(value="/image/{id}",consumes="multipart/form-data")
    ProduitDto upload(@RequestParam(value="image") MultipartFile file,@PathVariable long id) throws IOException {
        ProduitDto p=service.getProduitById(id);
        p.setImage(file.getBytes());
        return service.saveOrUpdate(p);
    }
    
    @PutMapping(consumes="application/json",produces="application/json")
    public ProduitDto update(@RequestBody ProduitDto prodDto) {
        return service.saveOrUpdate(prodDto);
    }
    
    
    
    @ResponseStatus(code=HttpStatus.NOT_FOUND)
    @ExceptionHandler({EmptyResultDataAccessException.class})
    public String handlerException(EmptyResultDataAccessException e) {
        return e.getMessage();
    }
    
    @GetMapping("/exception/io")
    public void  genIoException() throws IOException{
        throw new IOException("Traitement produit");
    }
    
    @GetMapping("/exception/exception")
    public void  genException() throws Exception{
        throw new Exception("Traitement produit");
    }
    
}
