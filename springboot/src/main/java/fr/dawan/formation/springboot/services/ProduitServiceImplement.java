package fr.dawan.formation.springboot.services;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import fr.dawan.formation.springboot.dto.ProduitDto;
import fr.dawan.formation.springboot.entities.Produit;
import fr.dawan.formation.springboot.repositories.ProduitRepository;

@Service
public class ProduitServiceImplement implements ProduitService {

    @Autowired
    ProduitRepository repository;

    @Autowired
    ModelMapper mapper;

    @Override
    public List<ProduitDto> getAllProduit(Pageable page) {
        Page<Produit> lst = repository.findAll(page);
        return lst.stream().map(p -> mapper.map(p, ProduitDto.class)).collect(Collectors.toList());
    }

    @Override
    public ProduitDto getProduitById(long id) {
        return mapper.map(repository.findById(id).get(), ProduitDto.class);
    }

    @Override
    public List<ProduitDto> getProduitByDescription(String Description) {
        return repository.findByDescriptionLikeIgnoreCase(Description).stream()
                .map(p -> mapper.map(p, ProduitDto.class)).collect(Collectors.toList());
    }

    @Override
    public void deleteProduit(long id) {
        repository.deleteById(id);
        
    }
    
    public ProduitDto saveOrUpdate(ProduitDto prod) {
        Produit p=repository.saveAndFlush(mapper.map(prod, Produit.class));
        return mapper.map(p, ProduitDto.class);
    }

}
