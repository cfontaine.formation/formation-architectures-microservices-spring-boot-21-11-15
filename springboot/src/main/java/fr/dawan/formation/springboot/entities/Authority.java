package fr.dawan.formation.springboot.entities;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.security.core.GrantedAuthority;
@Entity
public class Authority implements GrantedAuthority {

    private static final long serialVersionUID = 1L;
    @Id
    private String authorithy;
    
    @Override
    public String getAuthority() {
        return authorithy;
    }

    public void setAuthorithy(String authorithy) {
        this.authorithy = authorithy;
    }
    
}
