package fr.dawan.formation.springboot.services;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import fr.dawan.formation.springboot.dto.MarqueDto;
import fr.dawan.formation.springboot.entities.Marque;
import fr.dawan.formation.springboot.repositories.MarqueRepository;

@Service
public class MarqueServiceImplement implements MarqueService {

    @Autowired
    MarqueRepository repository;

    @Autowired
    ModelMapper mapper;

    @Override
    public List<MarqueDto> getAllMarque() {
        return repository.findAll().stream().map(m -> mapper.map(m, MarqueDto.class)).collect(Collectors.toList());
    }

    @Override
    public List<MarqueDto> getAllMarque(Pageable page) {
        return repository.findAll(page).stream().map(m -> mapper.map(m, MarqueDto.class)).collect(Collectors.toList());
    }

    @Override
    public MarqueDto saveOrUpdate(MarqueDto md) {
        Marque m = repository.saveAndFlush(mapper.map(md, Marque.class));
        return mapper.map(m, MarqueDto.class);
    }

    @Override
    public MarqueDto getMarqueById(long id) {
        return mapper.map(repository.findById(id).get(), MarqueDto.class);
    }

    @Override
    public void deleteById(long id) {
        repository.deleteById(id);

    }

}
