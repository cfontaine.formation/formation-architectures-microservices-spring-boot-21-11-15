package fr.dawan.formation.springboot.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name="maires")
public class Maire implements Serializable {
    
    private static final long serialVersionUID = 1L;

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private long id;

        private String prenom;
        
        private String nom;

        @OneToOne(mappedBy="maire")
        private Commune ville;
        
        @Version
        private int version;

        public Maire(String prenom, String nom) {
            this.prenom = prenom;
            this.nom = nom;
        }

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getPrenom() {
            return prenom;
        }

        public void setPrenom(String prenom) {
            this.prenom = prenom;
        }

        public String getNom() {
            return nom;
        }

        public void setNom(String nom) {
            this.nom = nom;
        }

        @Override
        public String toString() {
            return "Maire [id=" + id + ", prenom=" + prenom + ", nom=" + nom + "]";
        }
        
        
}
