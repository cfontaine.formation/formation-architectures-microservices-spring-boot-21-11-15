package fr.dawan.formation.springboot.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="coordonnees")
public class Coordonnee extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @Column(nullable = false)
    private double longitude;
    
    @Column(nullable = false)
    private double latitude;

    public Coordonnee() {
    }

    public Coordonnee(double longitude, double latitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    @Override
    public String toString() {
        return "Coordonnee [longitude=" + longitude + ", latitude=" + latitude + "]";
    }
    
}

