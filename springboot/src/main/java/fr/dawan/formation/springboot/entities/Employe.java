package fr.dawan.formation.springboot.entities;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Table;
@Entity
@Table(name="employes")
public class Employe extends AbstractEntity {

    private static final long serialVersionUID = 1L;
    
    private String prenom;
    
    private String nom;
    
    @Embedded
    private Adresse adressePersonnel;

    @Embedded
    @AttributeOverrides({
        @AttributeOverride(name = "rue", column = @Column(name="rue_pro")),
        @AttributeOverride(name = "ville", column = @Column(name="ville_pro")),
        @AttributeOverride(name = "codePostal", column = @Column(name="codePostal_pro"))
    })
    private Adresse adressePro;
    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Adresse getAdressePersonnel() {
        return adressePersonnel;
    }

    public void setAdressePersonnel(Adresse adressePersonnel) {
        this.adressePersonnel = adressePersonnel;
    }

    @Override
    public String toString() {
        return "Employe [prenom=" + prenom + ", nom=" + nom + ", adressePersonnel=" + adressePersonnel + ", adressePro="
                + adressePro + "]";
    }
    
    
    
}
