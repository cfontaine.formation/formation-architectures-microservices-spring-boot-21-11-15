INSERT INTO FOURNISSEURS (NOM,VERSION) VALUES
	 ('FournisseurA',0),
	 ('FournisseurB',0),
	 ('FournisseurC',0);

INSERT INTO MARQUE (NOM,VERSION) VALUES
	 ('MarqueA',0),
	 ('MarqueB',0),
	 ('MarqueC',0);
	 
INSERT INTO PRODUITS (CONDITIONNNEMENT,DESCRIPTION,PRIX,ID_MARQUE) VALUES
	 ('CARTON','TV 4K 60 pouces',799.9,1),
	 ('CARTON','Ecran 27 pouces',300.0,1),
	 ('CARTON','Four micro-onde',150.0,1),
	 ('CARTON','TV 4K 75pouces',1150.0,1),
	 ('PLASTIQUE','Souris',29.9,2),
	 ('CARTON','Clavier',9.99,2),
	 ('PLASTIQUE','Clé wifi',5.99,3),
	 ('CARTON','Smartphone',300.0,3),
	 ('PLASTIQUE','SSD 1go SATA',140.0,3),
	 ('CARTON','Carte Vidéo',450.0,3),
	 ('CARTON','Carte Mère',150.0,3),
	 ('PLASTIQUE','Téléphone Portable',15.0,3),
	 ('PLASTIQUE','Souris sans-fil',45.0,2),
	 ('CARTON','Routeur',100.0,2);

INSERT INTO PRODUITS2FOURNISSEURS (ID_PRODUIT,ID_FOURNISSEUR) VALUES
	 (1,1),
	 (9,2),
	 (1,3),
	 (1,2),
	 (3,1),
	 (2,2),
	 (7,2),
	 (12,2),
	 (13,3),
	 (14,2),
	 (4,1),
	 (4,3),
	 (11,2),
	 (6,1),
	 (8,3),
	 (5,2),
	 (5,3);