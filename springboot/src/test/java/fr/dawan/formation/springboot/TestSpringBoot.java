package fr.dawan.formation.springboot;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.dawan.formation.springboot.dto.MarqueDto;
import fr.dawan.formation.springboot.dto.ProduitDto;

@SpringBootTest()
@ActiveProfiles("TEST")
@AutoConfigureMockMvc(addFilters=false)
public class TestSpringBoot {
        
    @Autowired
    private MockMvc mockMvc;
    
    @Autowired
    private ObjectMapper mapper;
    
    @Test
    void testGetAll() {
        try {
            mockMvc.perform(get("/api/produits"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("[0].description",is("TV 4K 60 pouces")))
            .andExpect(jsonPath("[0].prix",is(799.9)));
        } catch (Exception e) {
            assertTrue(false);
            e.printStackTrace();
        }
                
    }
    
    
    @Test
    @DisplayName("Test de /api/produits et GET findByid")
    void testFindById() {
        try {
            String json=mockMvc.perform(get("/api/produits/1")).andReturn().getResponse().getContentAsString();
            mapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT,true);
            ProduitDto dto=mapper.readValue(json,ProduitDto.class);
            assertEquals("TV 4K 60 pouces",dto.getDescription());
            assertEquals(799.9,dto.getPrix());
        } catch (Exception e) {
            assertTrue(false);
            e.printStackTrace();
        }
        
    }
    
    @Test
    void testSave() {
        ProduitDto prd=new ProduitDto();
        prd.setDescription("testsave");
        prd.setPrix(120.0);
        MarqueDto md=new MarqueDto();
        md.setId(1);
        md.setNom("MarqueA");
        prd.setMarque(md);
        mapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
        try {
            String json=mapper.writeValueAsString(prd);
            
            String jsonR=mockMvc.perform(post("/api/produits")
                                            .contentType(MediaType.APPLICATION_JSON)
                                            .accept(MediaType.APPLICATION_JSON)
                                            .content(json)
                                             )
                    .andReturn().getResponse().getContentAsString();
            System.out.println(jsonR);
            ProduitDto resTest = mapper.readValue(jsonR, ProduitDto.class);
            assertNotEquals(0L, resTest.getId());
        } catch (Exception e) {
            e.printStackTrace();
            assertTrue(false);
        } 
    }


}
