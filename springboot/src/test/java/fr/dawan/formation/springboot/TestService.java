package fr.dawan.formation.springboot;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;

import fr.dawan.formation.springboot.dto.MarqueDto;
import fr.dawan.formation.springboot.services.MarqueService;
import fr.dawan.formation.springboot.services.ProduitService;

@SpringBootTest
@ActiveProfiles("DEV")
public class TestService {

    @Autowired
    MarqueService service;
    
    @Autowired
    ProduitService prodService;

    @Test
    void TestServiceMarque() {
        MarqueDto dto = new MarqueDto("MarqueD");
        MarqueDto res = service.saveOrUpdate(dto);
        System.out.println(res);
        System.out.println(service.getMarqueById(res.getId()));
        service.deleteById(res.getId());
        service.getAllMarque().forEach(System.out::println);
        
    }
    
    @Test
    void testServiceProduit() {
        prodService.getAllProduit(Pageable.unpaged()).forEach(System.out::println);
    }
}
