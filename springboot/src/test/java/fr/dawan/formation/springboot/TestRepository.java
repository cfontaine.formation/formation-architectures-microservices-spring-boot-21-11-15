package fr.dawan.formation.springboot;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import fr.dawan.formation.springboot.entities.Employe;
import fr.dawan.formation.springboot.repositories.EmployeCustomRepository;
import fr.dawan.formation.springboot.repositories.MarqueRepository;
import fr.dawan.formation.springboot.repositories.ProduitRepository;

@SpringBootTest
@ActiveProfiles("DEV")
public class TestRepository {

    @Autowired
    MarqueRepository marqueRepository;

    @Autowired
    ProduitRepository produitRepository;
    
    @Autowired 
    EmployeCustomRepository customRepository;
//    @Test
//    void testPersist() {
//        Marque mA=new Marque();
//        mA.setNom("MarqueB");
//        System.out.println(mA);
//        mA=marqueRepository.save(mA);
//        System.out.println(mA);
//        
//        List<Marque> lst=marqueRepository.findAll();
//        for(Marque m : lst ) {
//            System.out.println(m);
//        }
//        marqueRepository.deleteById((long) 2);
//        lst=marqueRepository.findAll();
//        for(Marque m : lst ) {
//            System.out.println(m);
//        }
//    }

//    @Test
//    void testFindByName() {
//        List<Marque> lst=marqueRepository.findByNom("MarqueA");
//        for(Marque m:lst) {
//            System.out.println(m);
//        }
//    }

 //   @Test
 //   void testProduit() {
//        List<Produit> lst=produitRepository.findByPrixLessThanEqual(400.0);
//        for(Produit p:lst) {
//            System.out.println(p);
//        }
//        
//        lst=produitRepository.findByPrixBetween(100.0,400.0);
//        for(Produit p:lst) {
//            System.out.println(p);
//        }
//        
//        lst=produitRepository.findByPrixInf(100.0);
//        for(Produit p:lst) {
//            System.out.println(p);
//        }
//        
//        lst=produitRepository.findByPrixInfJPQL(100.0);
//        for(Produit p:lst) {
//            System.out.println(p);
//        }

//        Page<Produit> p = produitRepository.findByPrixLessThanEqual(500.0, PageRequest.of(2, 2,Sort.by("prix")));
//        System.out.println(p.getTotalElements() + " " + p.getTotalPages());
//        List<Produit> lst = p.getContent();
//        for (Produit p1 : lst) {
//            System.out.println(p1);
//        }
//
//        p = produitRepository.findByPrixLessThanEqual(500.0, Pageable.unpaged());
//        System.out.println(p.getTotalElements() + " " + p.getTotalPages());
//        lst = p.getContent();
//        for (Produit p2 : lst) {
//            System.out.println(p2);
//        }
//
//        List<Produit> lstP = produitRepository.findByPrixLessThanEqual(500.0, Sort.by("prix").descending());
//        for (Produit p3 : lstP) {
//            System.out.println(p3);
//        }
//
//       lstP = produitRepository.findByPrixLessThanEqualOrderByPrix(400.0);
//        for (Produit p3 : lstP) {
//            System.out.println(p3);
//        }
        @Test
        void testCustomRepository()
        {
            List<Employe>lstE= customRepository.findBy(null, "Dalton");
            lstE.forEach(System.out::println);
            lstE= customRepository.findBy("Jo", null);
            lstE.forEach(System.out::println);
            
            System.out.println(produitRepository.countByPrix(20.0));
    }
}
