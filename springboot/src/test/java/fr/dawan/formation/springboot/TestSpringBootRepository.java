package fr.dawan.formation.springboot;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import fr.dawan.formation.springboot.entities.Produit;
import fr.dawan.formation.springboot.repositories.ProduitRepository;

@DataJpaTest
@ActiveProfiles("TEST")
public class TestSpringBootRepository {

    @Autowired
    private ProduitRepository repository;
    
//    @Autowired
//    TestEntityManager entityManager;
    
    @Test
    public void testRepository() {
        assertNotNull(repository);
    }
    
    @Test
    void testProduitRepositorySave() {
        Produit prod=new Produit("TEST2", 10.0);
        Produit res= repository.saveAndFlush(prod);
        assertNotEquals(0L, res.getId());
    }
    
    @Test
    public void testFindLessThan() {
//        entityManager.persistAndFlush(new Produit("test",6.0));
//        entityManager.persistAndFlush(new Produit("test",16.0));
        List<Produit> produits=repository.findByPrixLessThanEqualOrderByPrix(6.0);
        assertEquals(1, produits.size());
        assertEquals(5.99,produits.get(0).getPrix());
    }
}
