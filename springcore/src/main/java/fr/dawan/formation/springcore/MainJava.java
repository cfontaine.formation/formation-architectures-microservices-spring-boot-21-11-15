package fr.dawan.formation.springcore;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import fr.dawan.formation.springcore.beans.ArticleDao;
import fr.dawan.formation.springcore.beans.ArticleService;

public class MainJava {

    public static void main(String[] args) {
        ApplicationContext ctx=new AnnotationConfigApplicationContext(AppConfig.class);
        ArticleDao a=ctx.getBean("daoC1",ArticleDao.class);
        ArticleService s=ctx.getBean("serviceC",ArticleService.class);
        ArticleService sd=ctx.getBean("serviceD",ArticleService.class);
        System.out.println(a);
        System.out.println(s);
        System.out.println(sd);
    }

}
