package fr.dawan.formation.springcore;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.dawan.formation.springcore.beans.ArticleDao;
import fr.dawan.formation.springcore.beans.ArticleService;

public class MainAnotation {

    public static void main(String[] args) {
      ApplicationContext ctx= new ClassPathXmlApplicationContext("beans-anotation.xml");
     // ArticleDao a=ctx.getBean("daoA",ArticleDao.class);
     // System.out.println(a);
      ArticleService s=ctx.getBean("serviceA",ArticleService.class);
      System.out.println(s);
    }

}
