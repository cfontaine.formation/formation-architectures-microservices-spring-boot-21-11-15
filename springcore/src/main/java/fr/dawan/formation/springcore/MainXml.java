package fr.dawan.formation.springcore;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.dawan.formation.springcore.beans.ArticleDao;
import fr.dawan.formation.springcore.beans.ArticleService;

public class MainXml {

    public static void main(String[] args) {
        ApplicationContext ctx= new ClassPathXmlApplicationContext("beans.xml");
        System.out.println("--------------------------------");
        ArticleDao a=ctx.getBean("daoA", ArticleDao.class);
       // ArticleDao b=ctx.getBean("daoB", ArticleDao.class);
        System.out.println(a);
        //System.out.println(b);
        
        // Scope
        ArticleDao a2=ctx.getBean("daoA", ArticleDao.class);
        System.out.println(a2);
        // Référence
        ArticleService s1=ctx.getBean("serviceA",ArticleService.class);
        System.out.println(s1);

    }

}
