package fr.dawan.formation.springcore.beans;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class ArticleService implements Serializable {

    private static final long serialVersionUID = 1L;
    @Autowired
    private ArticleDao dao;

    public ArticleService() {
        super();
    }
    
    
    public ArticleService( ArticleDao dao) {
        super();
        this.dao = dao;
    }

    public ArticleDao getDao() {
        return dao;
    }

    
    public void setDao(ArticleDao dao) {
        this.dao = dao;
    }

    @Override
    public String toString() {
        return "ArticleService [dao=" + dao + "]";
    }
}
