package fr.dawan.formation.springcore;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import fr.dawan.formation.springcore.beans.ArticleDao;
import fr.dawan.formation.springcore.beans.ArticleService;

@Configuration
@ComponentScan(basePackages = "fr.dawan.formation.springcore")
public class AppConfig {
    
    @Bean(name= "daoC1")
    public ArticleDao daoC() {
        return new ArticleDao("data","url");
    }

    
    @Bean
    public ArticleService serviceC() {
        return new ArticleService();
    }
    
    @Bean
    public ArticleService serviceD(ArticleDao daoC1){
        return new ArticleService(daoC1);
    }
}
