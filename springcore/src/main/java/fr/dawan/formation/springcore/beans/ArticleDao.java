package fr.dawan.formation.springcore.beans;

import java.io.Serializable;

import org.springframework.stereotype.Component;

//@Component
public class ArticleDao implements Serializable {


    private static final long serialVersionUID = 1L;

    
    private String data;

    private String url;
    
    public ArticleDao() {
        super();
        System.out.println("Constructeur par défaut ArticleDao");
    }


    public ArticleDao(String data) {
        super();
        this.data = data;
        System.out.println("Constructeur 1 paramètre ArticleDao");
    }


    public ArticleDao(String data, String url) {
        super();
        this.data = data;
        this.url = url;
        System.out.println("Constructeur 2 paramètres ArticleDao");
    }


    public String getData() {
        return data;
    }


    public void setData(String data) {
        System.out.println("setter data");
        this.data = data;
    }


    public String getUrl() {
        return url;
    }


    public void setUrl(String url) {
        this.url = url;
    }


    @Override
    public String toString() {
        return "ArticleDao [data=" + data + ", url=" + url + ", toString()=" + super.toString() + "]";
    }





    
}
