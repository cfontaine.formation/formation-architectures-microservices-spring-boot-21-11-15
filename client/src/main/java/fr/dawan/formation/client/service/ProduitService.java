package fr.dawan.formation.client.service;

import java.util.List;

import fr.dawan.formation.client.beans.ProduitDto;

public interface ProduitService {
    List<ProduitDto> findAll(int page, int size);
}
