package fr.dawan.formation.client.service;

import java.net.URI;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.dawan.formation.client.beans.ProduitDto;

@Service
public class ProduitServiceImpl implements ProduitService {

    @Override
    public List<ProduitDto> findAll(int page, int size) {

        if (size == 0)
            size = 10;

        List<ProduitDto> lRes = null;
        RestTemplate restTemplate = new RestTemplate();
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);

        URI url;
        try {
            url = new URI("http://localhost:8081/api/produits?page=" + page + "&size=" + size);
            ResponseEntity<String> repWs = restTemplate.getForEntity(url, String.class);
            if (repWs.getStatusCode() == HttpStatus.OK) {
                String json = repWs.getBody();
                ProduitDto[] resArray = objectMapper.readValue(json, ProduitDto[].class);
                lRes = Arrays.asList(resArray);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return lRes;
    }

}
