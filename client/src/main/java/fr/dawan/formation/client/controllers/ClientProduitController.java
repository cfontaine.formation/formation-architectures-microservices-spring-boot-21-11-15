package fr.dawan.formation.client.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import fr.dawan.formation.client.beans.ProduitDto;
import fr.dawan.formation.client.service.ProduitService;

@Controller
public class ClientProduitController {

    @Autowired
    ProduitService produitService;
    
    @GetMapping("/list")
    public String displayProducts(Model model,@RequestParam int page,@RequestParam int size) {
        page = page-1;
        List<ProduitDto> lRes = produitService.findAll(page,size);
        model.addAttribute("lstProds", lRes);
        model.addAttribute("page", (page+1));
        model.addAttribute("size", size);
        return "produits"; //template/produits.ftlh
    }
}
