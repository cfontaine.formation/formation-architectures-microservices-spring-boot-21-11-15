package fr.dawan.formation.client.beans;

public class ProduitDto {

    private long id;

    private String description;

    private double prix;

    private MarqueDto marque;

    private byte[] image;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public MarqueDto getMarque() {
        return marque;
    }

    public void setMarque(MarqueDto marque) {
        this.marque = marque;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "ProduitDto [id=" + id + ", description=" + description + ", prix=" + prix + "]";
    }

}
