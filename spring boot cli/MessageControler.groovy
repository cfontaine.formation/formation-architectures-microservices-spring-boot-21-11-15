@Controller
@Grab('spring-boot-starter-thymeleaf')

class MessageController {
   @RequestMapping("/message/{message}")
   
   String getMessage(Model model,@PathVariable message) {
      model.addAttribute("message", message);
      return "message";
   }
} 